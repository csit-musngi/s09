package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//this application will function as an endpoint that will be used in handling http request
@RestController
@RequestMapping ("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello() {
		return "Hello World";
	}

	@GetMapping("/hi")

	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}
	//multiple parameters
	// localhost:8080/friend?name=value&friend=value

	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	//route with path variables
	//Dynamic data is obtained directly from the url
	//localhost:8080/
	@GetMapping("/hello/{name}")
	//@PathVariable annotation allows us to extract directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s", name);
	}

	@GetMapping("/welcome")
	public String roleGreet(@RequestParam(value = "user", defaultValue = "Tolits") String user, @RequestParam(value="role", defaultValue = "teacher") String role) {
		if(role.equals("teacher")){
			return String.format("Thank you for logging in, Teacher %s!", user);
		}
		else if(role.equals("admin")){
			return String.format("Welcome back to the class portal, Admin %s!",user);
		}
		else if(role.equals("student")){
			return String.format("Welcome to the class portal, %s!", user);
		}
		else{
			return String.format("Role out of range!");
		}


	}


	ArrayList<Student> students = new ArrayList<Student>();

	@GetMapping("/register")
	public String registerStudent (@RequestParam(value="id") String id, @RequestParam(value="name", defaultValue = "Tolits") String name, @RequestParam(value="course", defaultValue = "CSIT343") String course){
		Student student = new Student(id, name, course);
		students.add(student);
		return String.format("%s your id number is registered on the system!", id);


	}

	@GetMapping("/account/{id}")
	public String searchStudent (@PathVariable ("id") String id){

		for(int i=0; i<students.size();i++) {
			if (students.get(i).getId().equals(id)) {
				return String.format("Welcome back %s! You are currently enrolled in %s.", students.get(i).getName(), students.get(i).getCourse());
			}
			/*else{
				return String.format("Your provided id %s is not found in the system!", id);
			}*/
		}
		return String.format("Your provided id %s is not found in the system!", id);

	}
}
